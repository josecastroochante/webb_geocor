from email import message
import boto3
from decouple import config
from math import cos, asin, sqrt
from boto3.dynamodb.conditions import Key, Attr
from decimal import Decimal
from random import random, choice
import jwt
import re



AWS_ACCESS_KEY_ID       = config("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY   = config("AWS_SECRET_ACCESS_KEY")
REGION_NAME             = config("REGION_NAME")
MAP_LIMIT_POINTS        = int(config("MAP_LIMIT_POINTS"))
SECRET_JWT_KEY          = config('SECRET_JWT_KEY')
ENVIRONMENT             = config('ENVIRONMENT')

client = boto3.client(
    'dynamodb',
    aws_access_key_id     = AWS_ACCESS_KEY_ID,
    aws_secret_access_key = AWS_SECRET_ACCESS_KEY,
    region_name           = REGION_NAME,
)

resource = boto3.resource(
    'dynamodb',
    aws_access_key_id     = AWS_ACCESS_KEY_ID,
    aws_secret_access_key = AWS_SECRET_ACCESS_KEY,
    region_name           = REGION_NAME,
)

AnswerTable = resource.Table(f'telegram-bot-goecor-answers-{ENVIRONMENT}')
DirectoryTable = resource.Table(f'telegram-bot-goecor-directory-{ENVIRONMENT}')
QuestionTable = resource.Table(f'telegram-bot-goecor-questions-{ENVIRONMENT}')

RequestTable = resource.Table(f'telegram-bot-goecor-requests-{ENVIRONMENT}')
StatusTable = resource.Table(f'telegram-bot-goecor-statusflow-{ENVIRONMENT}')



def get_bot_users():
    response = {'data': [], 'message': '', 'success': False}
    scan_kwargs = {
        #'FilterExpression': Attr('main_process_workflow').eq('1'),
        #'ProjectionExpression': "request_id, question_id, alternatives, question, type2"
        'ProjectionExpression': "phone_number, profile, chat_id, dni, local_id, name2, orce_id"
        #'ProjectionExpression': "chat_id, phone_number, destino, nombres, origen, vehiculo, main_process_workflow"
    }
    try:
        scan = DirectoryTable.scan(**scan_kwargs)
        print(scan)
        if scan['Count'] > 0:
            users = [{**it, 'selected':False, 'odpe': ODPE_CATALOG[it['destino']]} for it in scan['Items']]
            response['data'] = users
            response['success'] = True
        else:
            response['message'] = 'No hay usuarios registrados'
        return response
    except Exception as e:
        response['message'] = e
        print(f'Exception: {e}')
    else:
        return response
    return response