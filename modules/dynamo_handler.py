from email import message
import boto3
from decouple import config
from math import cos, asin, sqrt
from boto3.dynamodb.conditions import Key, Attr
from decimal import Decimal
from random import random, choice
import jwt
import re



AWS_ACCESS_KEY_ID       = config("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY   = config("AWS_SECRET_ACCESS_KEY")
REGION_NAME             = config("REGION_NAME")
MAP_LIMIT_POINTS        = int(config("MAP_LIMIT_POINTS"))
SECRET_JWT_KEY          = config('SECRET_JWT_KEY')
ENVIRONMENT             = config('ENVIRONMENT')

client = boto3.client(
    'dynamodb',
    aws_access_key_id     = AWS_ACCESS_KEY_ID,
    aws_secret_access_key = AWS_SECRET_ACCESS_KEY,
    region_name           = REGION_NAME,
)

resource = boto3.resource(
    'dynamodb',
    aws_access_key_id     = AWS_ACCESS_KEY_ID,
    aws_secret_access_key = AWS_SECRET_ACCESS_KEY,
    region_name           = REGION_NAME,
)

AnswerTable = resource.Table(f'telegram-bot-goecor-answers-{ENVIRONMENT}')
DirectoryTable = resource.Table(f'telegram-bot-goecor-directory-{ENVIRONMENT}')
QuestionTable = resource.Table(f'telegram-bot-goecor-questions-{ENVIRONMENT}')
RequestTable = resource.Table(f'telegram-bot-goecor-requests-{ENVIRONMENT}')
StatusTable = resource.Table(f'telegram-bot-goecor-statusflow-{ENVIRONMENT}')
