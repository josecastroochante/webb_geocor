import json
from urllib import response
from flask import Flask, request, render_template
from flask_cors import CORS, cross_origin
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField


app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

import modules.dynamodb_handler as dynamodb


app.config['SECRET_KEY'] = 'whatever'

class InfoForm(FlaskForm):
	tel_number = StringField("Enter you número de telefono")
	submit = SubmitField("Submit")


"""
perfil_result = dynamodb_table.query(KeyConditionExpression=Key("phone_number").eq(phone_desc))
    perfil_columns = ['dni','cargo','local_pk','name','orc_pf']#'phone_number',
    perfil = dict(zip(perfil_columns, ['']*len(perfil_columns)))
    if perfil_result['Count'] == 1:
        perfil = perfil_result['Items'][0]
    elif perfil_result['Count'] > 1:
        perfil = perfil_result['Items'][0]
        print('Solo debería haber un perfil!')
    for k_ in set(perfil_columns):# - set(message_data.user.keys()):
        message_data.user[k_] = perfil[k_]
    if perfil['cargo'] == '':
        message_data.user['cargo'] = catalog.PERFIL_OPERADOR_T2
    directory_columns = ['type', 'estado_despliegue', 'estado_repliegue', 'local_name']
    if perfil['cargo'] in (catalog.PERFIL_RLV, catalog.PERFIL_CORD_DISTRITAL) and message_data.user['orc_pf'] != '':
        directory_table = dynamodb_resource.Table(config.TELEGRAM_DIRECTORY_TABLE)
        directory_result = directory_table.query(KeyConditionExpression=Key("local_pk").eq(message_data.user['orc_pf']),
                                                 ProjectionExpression='#'+',#'.join(directory_columns),
                                                 ExpressionAttributeNames=dict([('#'+_, _) for _ in directory_columns]))
        message_data.user['orc_data'] = directory_result['Items'][0]
    directory = dict(zip(directory_columns, ['']*len(directory_columns)))
"""


@app.route('/', methods=['GET', 'POST'])
def root_route():
	tel_number = False
	form = InfoForm()
	if form.validate_on_submit():
		tel_number = form.tel_number.data
		form.tel_number.data = ""
	#response = dynamodb.get_bot_users()
	#return 'Hello World........!!'
	return render_template('login.html', form=form, tel_number=tel_number)

if __name__ == '__main__':
    app.run(host='localhost', port=5001, debug=True)